import React, { useLayoutEffect } from 'react';
import { NavLink, Link, useRouteMatch } from 'react-router-dom';
import FeatherIcon from 'feather-icons-react';
import { TopMenuStyle } from './style';

const TopMenu = () => {
  const { path } = useRouteMatch();

  useLayoutEffect(() => {
    const active = document.querySelector('.strikingDash-top-menu a.active');
    const activeDefault = () => {
      const megaMenu = active.closest('.megaMenu-wrapper');
      const hasSubMenuLeft = active.closest('.has-subMenu-left');
      if (!megaMenu) {
        active.closest('ul').classList.add('active');
        if (hasSubMenuLeft) hasSubMenuLeft.closest('ul').previousSibling.classList.add('active');
      } else {
        active.closest('.megaMenu-wrapper').previousSibling.classList.add('active');
      }
    };
    window.addEventListener('load', active && activeDefault);
    return () => window.removeEventListener('load', activeDefault);
  }, []);

  const addParentActive = event => {
    document.querySelectorAll('.parent').forEach(element => {
      element.classList.remove('active');
    });
      document.querySelectorAll('.menuLink').forEach(element => {
          element.classList.remove('active');
      });

    const hasSubMenuLeft = event.currentTarget.closest('.has-subMenu-left');
    const megaMenu = event.currentTarget.closest('.megaMenu-wrapper');
    if (!megaMenu) {
      event.currentTarget.closest('ul').previousSibling.classList.add('active');
      if (hasSubMenuLeft) hasSubMenuLeft.closest('ul').previousSibling.classList.add('active');
    } else {
      event.currentTarget.closest('.megaMenu-wrapper').previousSibling.classList.add('active');
    }
  };
  const addActive = event => {
    document.querySelectorAll('.menuLink').forEach(element => {
      element.classList.remove('active');
    });
      document.querySelectorAll('.parent').forEach(element => {
          element.classList.remove('active');
      });

      event.currentTarget.classList.add('active');

  };
  return (
    <TopMenuStyle>
      <div className="strikingDash-top-menu">
        <ul>
          <li>
            <Link to="/" onClick={addActive} className={'menuLink'}>Home</Link>
          </li>
          <li className="has-subMenu">
            <NavLink to="/topics" onClick={addActive} className="parent menuLink">
              Topics
            </NavLink>
            <ul className="subMenu">
              <li>
                <NavLink to="topics/test" onClick={addParentActive} className="menuLink">
                  Test
                </NavLink>
              </li>
              <li className="has-subMenu-left">
                <NavLink to="/topics/hasSubmenu" className="parent menuLink">
                  Has Submenu
                </NavLink>
                <ul className="subMenu">
                  <li>
                    <NavLink onClick={addParentActive} to="/topics/submenu" className="menuLink">
                      Submenu
                    </NavLink>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li>
            <Link to="/Instructors" onClick={addActive} className={'menuLink'}>Instructors</Link>
          </li>
          <li>
            <Link to="/apply" onClick={addActive} className={'menuLink'}>Apply Instructor</Link>
          </li>
        </ul>
      </div>
    </TopMenuStyle>
  );
};

export default TopMenu;
