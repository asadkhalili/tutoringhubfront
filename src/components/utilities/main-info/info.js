import React, {useEffect, useState} from 'react';
import {InfoWraper} from './main-info-style';
import {Button} from "../../buttons/buttons";
import {Link} from "react-router-dom";
import {NavAuth, UserDropDwon} from "../auth-info/auth-info-style";
import {Dropdown} from "../../dropdown/dropdown";
import {getI18n} from "react-i18next";
import {Modal} from '../../modals/antd-modals';
import SignIn from '../../../container/profile/authentication/overview/SignIn';
import SignUp from "../../../container/profile/authentication/overview/Signup";
import ForgotPassword from "../../../container/profile/authentication/overview/ForgotPassword";
import {useDispatch, useSelector} from "react-redux";
import FeatherIcon from "feather-icons-react";
import Heading from "../../heading/heading";
import {Popover, Avatar} from "antd";
import {logOut} from '../../../redux/authentication/actionCreator';
import FontAwesome from "react-fontawesome";


const MainInfo = (props) => {
    const i18next = getI18n();
    const dispatch = useDispatch();
    var lang = 'en';
    if (i18next.language) {
        lang = i18next.language;
    }
    const popoverRef = React.createRef();
    const [state, setState] = useState({
        flag: lang,
        loginModalType: "signIn",
        userMenuShow: false
    });
    const {isLoggedIn, logoutLoading} = useSelector(state => {
        return {
            isLoggedIn: state.auth.login,
            logoutLoading: state.auth.loading
        };
    });
    const {flag} = state;
    const country = (
        <NavAuth>
            <Link onClick={() => onFlagChangeHandle('en')} to="#">
                <img src={require('../../../static/img/flag/en.png')} alt=""/>
                <span>English</span>
            </Link>
            <Link onClick={() => onFlagChangeHandle('fa')} to="#">
                <img src={require('../../../static/img/flag/fa.png')} alt=""/>
                <span>Persian / فارسی</span>
            </Link>
        </NavAuth>
    );
    const onFlagChangeHandle = value => {
        setState({
            ...state,
            flag: value,
        });
        i18next.changeLanguage(value);
        if (value === "fa") {
            const html = document.querySelector('html');
            html.setAttribute('dir', 'rtl');
            props.rtlChangerFunction(true);
        } else {
            const html = document.querySelector('html');
            html.setAttribute('dir', 'ltr');
            props.rtlChangerFunction(false);
        }
    };
    const [modalState, setModalState] = useState({visible: false, modalType: 'primary', colorModal: false});
    const loginModal = () => {
        setModalState({
            visible: true,
            modalType: "primary",
        });
    };

    useEffect(() => {
        if (isLoggedIn) {
            handleOk();
        }
    }, [isLoggedIn]);

    const handleOk = () => {
        setModalState({
            visible: false,
            colorModal: false,
        });
    };

    const handleCancel = () => {
        setModalState({
            visible: false,
            colorModal: false,
        });
    };

    const changeLoginForm = (value) => {
        setState({
            ...state,
            loginModalType: value
        });
    }

    const HandleLoginModal = () => {
        if (state.loginModalType === "signIn") {
            return <SignIn changeLoginForm={changeLoginForm}/>
        } else if (state.loginModalType === "signUp") {
            return <SignUp changeLoginForm={changeLoginForm}/>
        } else if (state.loginModalType === "forgetPassword") {
            return <ForgotPassword changeLoginForm={changeLoginForm}/>
        } else {
            return ''
        }
    }


    const SignOut = (e) => {
        e.preventDefault();
        dispatch(logOut());
    };
    const userContent = (
        <UserDropDwon>
            <div className="user-dropdwon">
                <figure className="user-dropdwon__info">
                    <img src={require('../../../static/img/avatar/chat-auth.png')} alt=""/>
                    <figcaption>
                        <Heading as="h5">Asad Khalili</Heading>
                        <p>UI Expert</p>
                    </figcaption>
                </figure>
                <ul className="user-dropdwon__links">
                    <li>
                        <Link to="#">
                            <FeatherIcon icon="settings"/> Settings
                        </Link>
                    </li>
                    <li>
                        <Link to="#">
                            <FeatherIcon icon="list"/> My Classes
                        </Link>
                    </li>
                    <li>
                        <Link to="#">
                            <FeatherIcon icon="dollar-sign"/> Billing
                        </Link>
                    </li>
                    <li>
                        <Link to="#">
                            <FeatherIcon icon="life-buoy"/> Support Ticket
                        </Link>
                    </li>
                </ul>
                <Link className="user-dropdwon__bottomAction" onClick={SignOut} to="#">
                    <FeatherIcon icon="log-out"/> {!logoutLoading ? "Sign Out" : 'loading ...'}
                </Link>
            </div>
        </UserDropDwon>
    );

    return (
        <InfoWraper>
            <div className="nav-author">
                <Dropdown placement="bottomRight" content={country} trigger="click">
                    <Link to="#" className="head-example">
                        <img src={require(`../../../static/img/flag/${flag}.png`)} alt=""/>
                    </Link>
                </Dropdown>
            </div>
            {!isLoggedIn ? <Button onClick={loginModal} className="btn-outlined" size="default" outlined shape="circle"
                                   type="light">Login / SingUp</Button> : <div className="nav-author">
                <Popover ref={popoverRef} placement="bottomRight" content={userContent} action="click">
                    <Button to="#" outlined shape="circle" type="dashed" size="default" className="head-example">
                        Asad Khalili
                        <Avatar src="https://cdn0.iconfinder.com/data/icons/user-pictures/100/matureman1-512.png"/>
                        <FontAwesome name="angle-down"/>
                    </Button>
                </Popover>
            </div>}
            <Modal
                type="primary"
                title="Login / SingUp"
                visible={modalState.visible}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
            >
                <HandleLoginModal/>
            </Modal>
        </InfoWraper>
    );
};

export default MainInfo;
