const {useState,useEffect} = require('react');

export default function connectApi() {
    //type = type || 'GET';
    const [data, setData] = useState([]);
    useEffect(() => {
        axios
            .get("https://jsonplaceholder.typicode.com/users")
            .then(result => setData(result.data));
    }, []);
};