import Cookies from 'js-cookie';
import actions from './actions';
import {DataService} from "../../config/dataService/dataService";
import {API} from "../../config/api";

const {loginBegin, loginSuccess, loginErr, logoutBegin, logoutSuccess, logoutErr} = actions;

const login = (email, password) => {
    return async dispatch => {
        try {
            dispatch(loginBegin());
            DataService.post(API.auth.login, {username: email, password: password})
                .then(function (response) {
                    console.log(response);
                    console.log('response');
                    if (response.status === 200) {
                        Cookies.set('loggedIn', true);
                        return dispatch(loginSuccess(true));
                    }else{
                        return dispatch(loginErr("Email or password is wrong!"));
                    }
                })
                .catch(function (error) {
                    // handle error
                    console.log('error1');
                    console.log(error);
                    DataService.errorHandler(error);
                });
        } catch (err) {
            console.log('error');
            dispatch(loginErr(err));
        }
    };
};

const logOut = () => {
    return async dispatch => {
        try {
            dispatch(logoutBegin());
            setTimeout(() => {
                Cookies.remove('logedIn');
                dispatch(logoutSuccess(null));
            }, 1000);
        } catch (err) {
            dispatch(logoutErr(err));
        }
    };
};

export {login, logOut};
