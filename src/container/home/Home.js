import React, {Fragment, useEffect, useState} from 'react';
import {Main} from '../styled';
import {Row, Col} from 'antd';
import {PageHeader} from '../../components/page-headers/page-headers';
import {DataService} from "../../config/dataService/dataService";


const Home = () => {
    const [data, setData] = useState(null);
    useEffect(() => {
        setData(DataService.get('/'));
    }, []);
    useEffect(() => {
    }, [data]);

    return (
        <Fragment>
            <PageHeader
                title="Home Page"
            />
            <Main>
                <Row>
                    <Col xs={6}>1</Col>
                    <Col xs={6}>2</Col>
                    <Col xs={6}>3</Col>
                    <Col xs={6}>4</Col>
                </Row>
            </Main>
        </Fragment>);
};

export default Home;