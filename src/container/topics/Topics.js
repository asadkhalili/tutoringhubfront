import React, {Fragment, lazy, Suspense} from 'react';
import { Main } from '../styled';
import {Row, Col, Skeleton} from 'antd';
import { PageHeader } from '../../components/page-headers/page-headers';
import {Route, useRouteMatch} from "react-router-dom";
import {Cards} from "../../components/cards/frame/cards-frame";

const Filters = lazy(() => import('./topicCard/Filters'));
const List = lazy(() => import('./topicCard/List'));

const Topics = () => {
    const { path } = useRouteMatch();

    return (
        <Fragment>
            <PageHeader
                title="Topics Page"
            />
            <Main>
                <Row gutter={30}>
                    <Col className="product-sidebar-col" xxl={5} xl={7} lg={7} md={10} xs={24}>
                        <Suspense
                            fallback={
                                <Cards headless>
                                    <Skeleton paragraph={{ rows: 22 }} active />
                                </Cards>
                            }
                        >
                            <Filters />
                        </Suspense>
                    </Col>
                    <Col className="product-content-col" xxl={19} lg={17} md={14} xs={24}>
                        <Suspense
                            fallback={
                                <div>
                                    <Cards headless>
                                        <Skeleton paragraph={{ rows: 2 }} active />
                                    </Cards>
                                    <Cards headless>
                                        <Skeleton paragraph={{ rows: 2 }} active />
                                    </Cards>
                                    <Cards headless>
                                        <Skeleton paragraph={{ rows: 2 }} active />
                                    </Cards>
                                    <Cards headless>
                                        <Skeleton paragraph={{ rows: 2 }} active />
                                    </Cards>
                                </div>
                            }
                        >
                        <List/>
                        </Suspense>
                    </Col>
                </Row>
            </Main>
        </Fragment>);
};

export default Topics;