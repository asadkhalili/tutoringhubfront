import React from 'react';
import {Rate, Row, Col, Divider} from 'antd';
import FeatherIcon from 'feather-icons-react';
import {useDispatch} from 'react-redux';
import {Link, NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import Heading from '../../../components/heading/heading';
import {Button} from '../../../components/buttons/buttons';
import {ProductCard} from '../Style';
import {updateWishList} from '../../../redux/product/actionCreator';

const ProductCardsList = ({product}) => {
    const {id, name, rate, price, oldPrice, popular, img, description} = product;
    const dispatch = useDispatch();

    return (
        <ProductCard className="list-view" style={{marginBottom: 20}}>
            <div className="product-list">
                <Row gutter={15}>
                    <Col md={18} xs={48}>
                        <Row gutter={15}>
                            <Col md={7} xs={24}>
                                <figure>
                                    <img style={{width: '100%'}} src={require(`../../../${img}`)} alt=""/>
                                </figure>
                            </Col>
                            <Col md={17} xs={24}>
                                <div className="product-single-description">
                                    <Heading className="product-single-title" as="h5">
                                        <NavLink to={`/profile/${id}`}>{name}</NavLink>
                                        <>
                                            <div className="product-single-sub-title">
                      <span>
                        Students: <strong>123</strong>
                      </span>
                                                <Divider type="vertical"/>
                                                <span>
                        Language: <strong>English</strong>
                      </span>
                                            </div>
                                        </>
                                    </Heading>
                                    <p>{description}</p>
                                </div>
                            </Col>
                        </Row>
                        <Button shape="circle" className="product-single-experience" size="extra-small" outlined
                                type="primary">
                            PHP
                        </Button>
                        <Button shape="circle" className="product-single-experience" size="extra-small" outlined
                                type="primary">
                            MYSQL
                        </Button>
                        <Button shape="circle" className="product-single-experience" size="extra-small" outlined
                                type="primary">
                            React
                        </Button>
                        <Button shape="circle" className="product-single-experience" size="extra-small" outlined
                                type="primary">
                            HTML
                        </Button>
                    </Col>
                    <Col md={6} xs={24}>
                        <div className="product-single-info">
                            <Link onClick={() => dispatch(updateWishList(id))} className="btn-heart" to="#">
                                <FeatherIcon
                                    icon="heart"
                                    size={14}
                                    color={popular ? '#FF4D4F' : '#9299B8'}
                                    fill={popular ? '#FF4D4F' : 'none'}
                                />
                            </Link>
                            <p className="product-single-price">
                                <span className="product-single-price__new">${price}</span>
                                {oldPrice && (
                                    <>
                                        <del> ${oldPrice} </del>
                                        <span className="product-single-price__offer"> 60% Off</span>
                                    </>
                                )}
                                <small> (Per/Hour)</small>
                            </p>
                            <div className="product-single-rating">
                                <Rate allowHalf defaultValue={rate} disabled/> 4.9
                                <span className="total-reviews"> 778 Reviews</span>
                            </div>
                            <div className="product-single-action">
                                    <Button className="btn-cart ant-btn-block" size="small" type="white" outlined>
                                        <NavLink to={`/profile/${id}`}>
                                        <FeatherIcon icon="user" size={14}/>
                                        View Profile
                                        </NavLink>
                                    </Button>
                                <Button className="btn-cart ant-btn-block" size="small" type="white" outlined>
                                    <FeatherIcon icon="play" size={14}/>
                                    Introduction Video
                                </Button>
                                <Button size="small" className="ant-btn-block" type="primary">
                                    Reserve Class
                                </Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        </ProductCard>
    );
};

ProductCardsList.propTypes = {
    product: PropTypes.object,
};

export default ProductCardsList;
