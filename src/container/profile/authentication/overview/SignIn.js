import React, {useState} from 'react';
import {Link, NavLink, useHistory} from 'react-router-dom';
import {Form, Input, Button} from 'antd';
import {useDispatch, useSelector} from 'react-redux';
import {AuthWrapper} from './style';
import {login} from '../../../../redux/authentication/actionCreator';
import {Checkbox} from '../../../../components/checkbox/checkbox';
import Heading from '../../../../components/heading/heading';

const SignIn = (props) => {
    const history = useHistory();
    const dispatch = useDispatch();
    const isLoading = useSelector(state => state.auth.loading);
    const isLoggedIn = useSelector(state => state.auth.login);
    const [form] = Form.useForm();
    const [state, setState] = useState({
        checked: null,
        loginResult: null
    });

    const handleSubmit = (values) => {
        //setState({loginResult: DataService.post(API.auth.login, {username: values.email, password: values.password})});

        dispatch(login(values.email, values.password));
        // history.push('/settings');
    };

    const onChange = checked => {
        setState({...state, checked});
    };
    const changeSignUp = () => {
        props.changeLoginForm('signUp');
    }
    const changeForget = () => {
        props.changeLoginForm('forgetPassword');
    }

    return (
        <AuthWrapper>
            <p className="auth-notice">
                Don&rsquo;t have an account?
                <Button onClick={changeSignUp}>Sign up now</Button>
            </p>
            <div className="auth-contents">
                <Form name="login" form={form} onFinish={handleSubmit} layout="vertical">
                    <Heading as="h3">
                        Sign in
                    </Heading>
                    <Form.Item
                        name="email"
                        rules={[{message: 'Please input your Email!', required: true}]}
                        label="Email Address"
                    >
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        rules={[{message: 'Please input your Password!', required: true}]}
                        name="password"
                        label="Password">
                        <Input.Password/>
                    </Form.Item>
                    <div className="auth-form-action">
                        <Checkbox onChange={onChange}>Keep me logged in</Checkbox>
                        <NavLink className="forgot-pass-link" to="#" onClick={changeForget}>
                            Forgot password?
                        </NavLink>
                    </div>
                    <Form.Item>
                        <Button className="btn-signin" htmlType="submit" type="primary" size="large">
                            {isLoading ? 'Loading...' : 'Sign In'}
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </AuthWrapper>
    );
};

export default SignIn;
