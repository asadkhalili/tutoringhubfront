import React, {lazy, Suspense, useState} from "react";
import FeatherIcon from "feather-icons-react";
import {PageHeader, Col, Row, Skeleton} from "antd";
import {Link, NavLink, Route, Switch, useRouteMatch} from "react-router-dom";
import {Cards} from "../../components/cards/frame/cards-frame";
import { SettingWrapper } from './overview/style';
import { Main } from '../styled';
import ModalVideo from "react-modal-video";
import {RightAsideWrapper} from "./myProfile/overview/style";
import './myProfile/overview/video-modal.css';

const UserCards = lazy(() => import('../overview/userCard/UserCard'));
const CoverSection = lazy(() => import('./overview/CoverSection'));
const UserBio = lazy(() => import('./myProfile/overview/UserBio'));
const WeekCalendar = lazy(() => import('../calendar/CalendarUser'));

const Profile = () => {
    const { path } = useRouteMatch();
    const [isOpen, setOpen] = useState(false);

    return (
        <>
            <PageHeader
                ghost
                title="Instructor Profile"
            />

            <Main>
                <RightAsideWrapper>
                    <ModalVideo channel="youtube" autoplay isOpen={isOpen} videoId="L61p2uyiMSo" onClose={() => setOpen(false)} />
                </RightAsideWrapper>
                <Row gutter={25}>
                    <Col xxl={6} lg={8} md={10} xs={24}>
                        <Suspense
                            fallback={
                                <Cards headless>
                                    <Skeleton avatar active paragraph={{ rows: 3 }} />
                                </Cards>
                            }
                        >
                            <UserCards
                                user={{ name: 'Duran Clyton', designation: 'UI/UX Designer', img: 'static/img/products/2.png' ,reserve:true}}
                            />
                            <RightAsideWrapper>
                            <Cards title="Interview Video">
                                <div className="widget-video-list">
                                    <Link onClick={() => setOpen(true)} className="video" to="#">
                                        <img style={{ width: '100%' }} src={require(`../../static/img/gallery/video.png`)} alt="" />
                                        <span>
                        <FeatherIcon icon="play" />
                      </span>
                                    </Link>
                                </div>
                            </Cards>
                            </RightAsideWrapper>
                        </Suspense>
                    </Col>
                    <Col xxl={18} lg={16} md={14} xs={24}>
                        <SettingWrapper>
                            <Suspense
                                fallback={
                                    <Cards headless>
                                        <Skeleton active />
                                    </Cards>
                                }
                            >
                                <div className="coverWrapper">
                                    <CoverSection />
                                </div>
                            </Suspense>
                            <Suspense
                                fallback={
                                    <Cards headless>
                                        <Skeleton active paragraph={{ rows: 10 }} />
                                    </Cards>
                                }
                            >
                                <UserBio />
                                <WeekCalendar/>
                            </Suspense>
                        </SettingWrapper>
                    </Col>
                </Row>
            </Main>
        </>
    );
}

export default Profile;