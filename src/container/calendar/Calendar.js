import React, {lazy, Suspense} from 'react';
import {Skeleton} from 'antd';
import {CalendarWrapper} from './Style';
import {Cards} from '../../components/cards/frame/cards-frame';
import 'react-calendar/dist/Calendar.css';

const WeekCalendar = lazy(() => import('./overview/Week'));

const Calendars = () => {

    return (
        <>
            <CalendarWrapper>
                <Suspense
                    fallback={
                        <Cards headless>
                            <Skeleton paragraph={{rows: 15}} active/>
                        </Cards>
                    }
                >
                    <WeekCalendar/>
                </Suspense>
            </CalendarWrapper>
        </>
    );
};

export default Calendars;
