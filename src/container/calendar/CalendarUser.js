import React, {lazy, Suspense, useState} from 'react';
import {Table, Col, Row, Skeleton} from 'antd';
import {CalendarWrapper} from './Style';
import {Cards} from '../../components/cards/frame/cards-frame';
import 'react-calendar/dist/Calendar.css';
import {Main, TableWrapper} from "../styled";
import {Button} from "../../components/buttons/buttons";

const CalendarUser = () => {

    const dataSource = [
        {
            key: '1',
            Sunday: <Button type="primary" size="extra-small" outlined shape="circle">12</Button>,
            Monday: <Button type="primary" size="extra-small" outlined shape="circle">12:30</Button>,
            Wednesday: <Button type="primary" size="extra-small" outlined shape="circle">13:30</Button>,
            Friday: <Button type="primary" size="extra-small" outlined shape="circle">10:00</Button>,
            Saturday: <Button type="primary" size="extra-small" outlined shape="circle">12:00</Button>,
        },
        {
            key: '2',
            Sunday: <Button type="primary" size="extra-small" outlined shape="circle">13:30</Button>,
        },
        {
            key: '2',
            Sunday: <Button type="primary" size="extra-small" outlined shape="circle">14:15</Button>,
        },

    ];

    const columns = [
        {
            title: "Sunday",
            dataIndex: 'Sunday',
            key: 'Sunday',
        },
        {
            title: 'Monday',
            dataIndex: 'Monday',
            key: 'Monday',
        },
        {
            title: 'Tuesday',
            dataIndex: 'Tuesday',
            key: 'Tuesday',
        },
        {
            title: 'Wednesday',
            dataIndex: 'Wednesday',
            key: 'Wednesday',
        },
        {
            title: 'Thursday',
            dataIndex: 'Thursday',
            key: 'Thursday',
        },
        {
            title: 'Friday',
            dataIndex: 'Friday',
            key: 'Friday',
        },
        {
            title: 'Saturday',
            dataIndex: 'Saturday',
            key: 'Saturday',
        },
    ];


    return (
        <>
            <CalendarWrapper>
                <Suspense
                    fallback={
                        <Cards headless>
                            <Skeleton paragraph={{rows: 15}} active/>
                        </Cards>
                    }
                >
                    <Cards title="Instructor Calendar">
                        <Table dataSource={dataSource} columns={columns} pagination={false}
                               style={{width: "100%", overflowX: "auto"}}/>
                    </Cards>
                </Suspense>
            </CalendarWrapper>
        </>
    );
};

export default CalendarUser;
