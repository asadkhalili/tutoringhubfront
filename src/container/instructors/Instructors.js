import React, {Fragment, lazy, Suspense, useState} from 'react';
import {CardToolbox, Main} from '../styled';
import { UsercardWrapper } from '../overview/style';
import {Row, Col, Skeleton, Pagination} from 'antd';
import { PageHeader } from '../../components/page-headers/page-headers';
import {Cards} from "../../components/cards/frame/cards-frame";
import {useSelector} from "react-redux";

const User = lazy(() => import('../overview/userCard/UserCard'));

const Instructors = () => {
    const { searchData, users } = useSelector(state => {
        return {
            searchData: state.headerSearchData,
            users: state.users,
        };
    });

    const [state, setState] = useState({
        notData: searchData,
        current: 0,
        pageSize: 0,
        page: 0,
    });

    const { notData } = state;

    const handleSearch = searchText => {
        const data = searchData.filter(item => item.title.toUpperCase().startsWith(searchText.toUpperCase()));
        setState({
            ...state,
            notData: data,
        });
    };

    const onShowSizeChange = (current, pageSize) => {
        setState({ ...state, current, pageSize });
    };

    const onChange = page => {
        setState({ ...state, page });
    };

    return (
        <>
            <CardToolbox>
                <PageHeader
                    ghost
                    title="Instructors"
                    subTitle={
                        <>
                            <span className="title-counter">274 Instructors </span>
                        </>
                    }
                />
            </CardToolbox>
            <Main>
                <UsercardWrapper>
                    <Row gutter={25}>
                        {users.map(user => {
                            const { id } = user;

                            return (
                                <Col key={id} xxl={6} xl={8} sm={12} xs={24}>
                                    <Suspense
                                        fallback={
                                            <Cards headless>
                                                <Skeleton avatar active />
                                            </Cards>
                                        }
                                    >
                                        <User user={user} />
                                    </Suspense>
                                </Col>
                            );
                        })}
                        <Col xs={24}>
                            <div className="user-card-pagination">
                                <Pagination
                                    onChange={onChange}
                                    showSizeChanger
                                    onShowSizeChange={onShowSizeChange}
                                    defaultCurrent={6}
                                    total={500}
                                />
                            </div>
                        </Col>
                    </Row>
                </UsercardWrapper>
            </Main>
        </>
    );
};

export default Instructors;