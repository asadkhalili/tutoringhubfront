import React, {Fragment, useEffect, useState} from 'react';
import {Main} from '../styled';
import {Row, Col, Spin} from 'antd';
import {PageHeader} from '../../components/page-headers/page-headers';
import {DataService} from '../../config/dataService/dataService';
import {NotificationApi} from "antd/lib/notification";
import {API} from "../../config/api";


const Apply = () => {
    const [loading, setLoading] = useState(true);
    const [didLoad, setDidLoad] = useState(false);
    useEffect( () => {
        if(!didLoad) {
            DataService.get(API.posts.posts)
                .then(function (response) {
                    if(response.status === 200){
                        setDidLoad(true);
                    }
                    setLoading(false);
                })
                .catch(function (error) {
                    // handle error
                    DataService.errorHandler(error);
                });
        }
    }, []);


    return (
        <Fragment>
            <PageHeader
                title="Apply Instructors Page"
            />
            <Main>
                {(loading) ? <Spin/> : null}
                <Row>
                    <Col xs={6}>1</Col>
                    <Col xs={6}>2</Col>
                    <Col xs={6}>3</Col>
                    <Col xs={6}>4</Col>
                </Row>
            </Main>
        </Fragment>);
};
export default Apply;