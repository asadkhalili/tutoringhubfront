import React from 'react';
import PropTypes from 'prop-types';
import {UserCard} from '../style';
import {Cards} from '../../../components/cards/frame/cards-frame';
import Heading from '../../../components/heading/heading';
import {Link} from 'react-router-dom';
import {Button} from '../../../components/buttons/buttons';
import FeatherIcon from 'feather-icons-react';
import {Col, Row, Rate} from 'antd';
import {useTranslation} from "react-i18next";

const UserCards = ({user}) => {
    const {t} = useTranslation();
    const {name, designation, img, reserve} = user;
    var primaryButton;
    if(!reserve){
        primaryButton = <Link to="/profile"><Button size="default" type="white"><FeatherIcon icon="user" size={14}/>Profile</Button></Link>
    }else{
        primaryButton = <Button block size="default" type="primary"><FeatherIcon icon="user-check" style={{color:"#fff"}} size={14}/>Reserve Class</Button>
    }
    return (
        <UserCard>
            <div className="card user-card">
                <Cards headless>
                    <figure>
                        <img src={require(`../../../${img}`)} alt=""/>
                    </figure>
                    <figcaption>
                        <div className="card__content">
                            <Heading className="card__name" as="h6">
                                <Link to="/profile">{name}</Link>
                            </Heading>
                            <p className="card__designation" style={{marginBottom: "-10px"}}>{designation}</p>
                            <Rate defaultValue={3} disabled style={{marginBottom: "10px"}}/>
                        </div>

                        <div className="card__actions">
                            {primaryButton}
                            <Button size="default" type="white">
                                <FeatherIcon icon="heart" size={14}/>
                                Following
                            </Button>
                        </div>
                        <div className="card__info">
                            <Row gutter={15}>
                                <Col xs={8}>
                                    <div className="info-single">
                                        <Heading className="info-single__title" as="h2">
                                            $72,572
                                        </Heading>
                                        <p>Price (Per Hour)</p>
                                    </div>
                                </Col>
                                <Col xs={8}>
                                    <div className="info-single">
                                        <Heading className="info-single__title" as="h2">
                                            3,257
                                        </Heading>
                                        <p>{t('Following')}</p>
                                    </div>
                                </Col>
                                <Col xs={8}>
                                    <div className="info-single">
                                        <Heading className="info-single__title" as="h2">
                                            74
                                        </Heading>
                                        <p>Students</p>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </figcaption>
                </Cards>
            </div>
        </UserCard>
    );
};

UserCards.propTypes = {
    user: PropTypes.object,
};

export default UserCards;
