import React, { lazy } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Instructors = lazy(() => import('../../container/instructors/Instructors'));


const InstructorsRoute = () => {
  const { path } = useRouteMatch();
  return (
    <Switch>
      <Route path={`${path}/`} component={Instructors} />
    </Switch>
  );
};

export default InstructorsRoute;
