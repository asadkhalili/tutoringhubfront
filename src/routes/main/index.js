import React, { lazy, Suspense } from 'react';
import { Spin } from 'antd';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import mainLayout from '../../layout/MainLayout';

const Home = lazy(() => import('../../container/home/Home'));
const Topics = lazy(() => import('./topics'));
const Instructors = lazy(() => import('./instructors'));
const Apply = lazy(() => import('./apply'));
const Profile = lazy(() => import('../../container/profile/Profile'));
const Settings = lazy(() => import('../../container/settings/Settings'));


const Main = () => {
  const { path } = useRouteMatch();
    return (
    <Switch>
      <Suspense
        fallback={
          <div className="spin">
            <Spin />
          </div>
        }
      >
        <Route exact path="/" component={Home} />
        <Route path="/topics" component={Topics} />
        <Route exact path="/instructors" component={Instructors} />
        <Route path="/apply" component={Apply} />
        <Route path="/profile" component={Profile} />
        <Route path="/settings" component={Settings} />
      </Suspense>
    </Switch>
  );
};

export default mainLayout(Main);
