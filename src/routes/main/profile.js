import React, { lazy } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Apply = lazy(() => import('../../container/apply/Apply'));


const ApplyRoute = () => {
  const { path } = useRouteMatch();
  return (
    <Switch>
      <Route path={`${path}/`} component={Apply} />
    </Switch>
  );
};

export default ApplyRoute;
