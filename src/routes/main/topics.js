import React, { lazy } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const Topics = lazy(() => import('../../container/topics/Topics'));


const TopicsRoute = () => {
  const { path } = useRouteMatch();
  return (
    <Switch>
      <Route path={`${path}/`} component={Topics} />
    </Switch>
  );
};

export default TopicsRoute;
