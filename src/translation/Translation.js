import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import englishResource from './langs/en/translation.json';
import persianResource from './langs/fa/translation.json';

// the translations
// (tip move them in a JSON file and import them)
const resources = {
    en: {
        translation: englishResource
    },
    fa: {
        translation: persianResource
    }
};

i18n
    .use(LanguageDetector) // passes i18n down to react-i18next
    .use(initReactI18next)
    .init({
        resources,
        //debug: true,
        keySeparator: false, // we do not use keys in form messages.welcome
        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });

export default i18n;